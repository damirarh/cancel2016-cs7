﻿namespace RPG.Patterns
{
    interface IEnemy
    {
        int Health { get; set; }
    }
}
