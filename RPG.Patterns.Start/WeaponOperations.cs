﻿namespace RPG.Patterns
{
    static class WeaponOperations
    {
        static void Attack(this IWeapon weapon, IEnemy enemy)
        {
            if (weapon is Sword)
            {
                var sword = weapon as Sword;
                if (sword.Durability > 0)
                {
                    enemy.Health -= sword.Damage;
                    sword.Durability--;
                }
            }
            else if (weapon is Bow)
            {
                var bow = weapon as Bow;
                if (bow.Arrows > 0)
                {
                    enemy.Health -= bow.Damage;
                    bow.Arrows--;
                }
            }
        }

        static void Repair(this IWeapon weapon)
        {
            if (weapon is Sword)
            {
                var sword = weapon as Sword;
                sword.Durability += 100;
            }
        }
    }
}
