﻿namespace RPG.Patterns
{
    class Sword : IWeapon
    {
        public int Damage { get; set; }
        public int Durability { get; set; }
    }
}
